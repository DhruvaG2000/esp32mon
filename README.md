# esp32mon

A Heltec ESP32 wifi/ LoRa based computer CPU/GPU/RAM usage and temperature monitor using an OLED as the interface, and LAN WIFI for wireless connectivity. 

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Visuals

### Windows 11

![](visuals/esp32mon-preview.gif)

## Installation
Installation methods will defer for linux based systems and windows.

### Linux
It's fairly straightforward, just make sure you have python3 installed and then go to the linux directory and run the script using:
```sh
./cpu_temp_esp32
```

### Windows
It's a little more complex, you will have to download and install and run OpenHardwareMonitor.
After this just run the exe from ``windows/dist`` and you should be good to go.

## Usage
This project is aimed to make monitoring CPU/GPU temperature levels and usage when you do not have an additional display and have low screen real estate and still wish to omonitor the computer stats.
It can be used on any device from a Raspberry pi to a Linux server as long as it is on you LAN WiFi.

## Support
To contribute, just create a PR and I will review it :)

## Roadmap
- Add support for %ge usage of CPU
- Add GPU support
- Test on various platforms

## License
MIT License

## Project status
It is ready to use, but more features are always welcome.
